(function() {
	$(".binding_email .nextBtn").on("click", function() {
		$("section .binding_email").css("display", "none");
		$("section .verify_email").css("display", "block");
		$("section .binding_succeed").css("display", "none");
	})
	$(".verify_email .nextBtn").on("click", function() {
		$("section .binding_email").css("display", "none");
		$("section .verify_email").css("display", "none");
		$("section .binding_succeed").css("display", "block");
	})
})();