/***
 * 
 * @param {Object} speed				移动速度			默认：500毫秒
 * @param {Object} interval				间隔时间  			默认：10000毫秒
 * @param {Object} isAuto				自动播放状态		默认：false
 * @param {Object} showIndicators		显示播放导航		默认：false
 * @param {Object} showControl			显示控制按钮		默认：false
 * 
 */
(function($) {
	$.fn.carousel = function(param) {
		param = $.extend({}, $.fn.carousel.defaults, param || {});
		
		var thisCarousel	= $(this),										//轮播控件
			thisInner		= $(this).find(".inner"),						//轮播内容
			thisInnerItem	= $(this).find(".inner").children(".item"),		//轮播内容项
			pageLength		= 0,											//轮播内容长度
			pageWidth		= 0,											//轮播内容宽度
			thisScroll		= 0,											//滚动位置
			thisIndex		= 0,											//索引
			process			= null;											//移动过程
		
		function init() {
			if (thisCarousel.length == 0) {
				alert("缺少对象！");
				return;
			}
			
			pageLength	= Math.ceil( thisInnerItem.length / param.playNum );
			pageWidth	= parseInt(thisInnerItem.css("width")) * param.playNum;
			thisIndex	= 1;
			
			/************************创建轮播前后副本************************/
			if (pageLength > 1) {
				if (thisInnerItem.length%param.playNum > 0) {
					for (var i=0; i<param.playNum-thisInnerItem.length%param.playNum; i++) {
						var empty = thisInnerItem.last().clone();
//						var empty = thisInnerItem.eq(i).clone();
						empty.html("");
						empty.appendTo(thisInner);
					}
					
//					thisInnerItem = thisInner.children(".item");
				}
				
				var firstPage	= thisInner.children( ".item:lt("+param.playNum+")" ),
					lastPage	= thisInner.children( ".item:not(:lt("+(thisInner.children(".item").length-param.playNum)+"))" );
				
				firstPage.clone().appendTo(thisInner);
				lastPage.clone().prependTo(thisInner);
				thisInner.css("margin-left", -pageWidth);
			}
			
			/************************创建轮播导航************************/
			if (param.showIndicators) {
				var indicators = $("<ol class=\"indicators\"></ol>");
				
				indicators.append("<li class=\"active\"></li>");
				for (var i=1; i<pageLength; i++) {
					indicators.append("<li></li>");
				}
				
				indicators.appendTo(thisCarousel);
			}
			
			thisCarousel.find(".indicators li").on("click", function() {
				thisIndex	= $(this).index() + 1;
				thisScroll	= thisIndex * pageWidth;
				changeInner();
			});
			
			/************************创建控制按钮************************/
			if (param.showControl) {
				var btnPrev = $("<i class=\"control prev\"><span class=\"iconfont icon-zuo\"></span></i>"),
					btnNext = $("<i class=\"control next\"><span class=\"iconfont icon-you\"></span></i>");
				
				btnPrev.appendTo(thisCarousel);
				btnNext.appendTo(thisCarousel);
			}
			
			thisCarousel.find(".prev").on("click", function() {
				prev();
			});
			thisCarousel.find(".next").on("click", function() {
				next();
			});
			
			if (param.isAuto) {
				autoPlay();
			}
		}
		
		/************************自动轮播************************/
		function autoPlay() {
			if (pageLength > 1) {
				process = setInterval(next, param.interval);
			}
		}
		
		/************************停止轮播************************/
		function stop() {
			clearInterval(process);
		}
		
		/************************上一个************************/
		function prev() {
			if (pageLength>1) {
				thisScroll = (thisIndex-1) * pageWidth;
				action();
			}
		}
		
		/************************下一个************************/
		function next() {
			if (pageLength>1) {
				thisScroll = (thisIndex+1) * pageWidth;
				action();
				console.log(new Date());
			}
		}
		
		/************************执行更换************************/
		function action() {
			if (thisScroll < pageWidth) {
				thisIndex = pageLength;
//				thisInner.children("li:not(:lt(" + ((pageLength-1)*param.playNum) + "))").clone().prependTo(thisInner);
//				thisInner.scrollLeft(pageWidth);
				thisInner.css("margin-left", -pageWidth + "px");
				thisScroll = 0;
				changeInner(function() {
//					thisInner.scrollLeft(thisIndex*pageWidth);
					thisInner.css("margin-left", -(thisIndex*pageWidth) + "px");
//					thisInner.children( "li:lt("+((pageLength-1)*param.playNum)+")" ).remove();
				});
			} else if (thisScroll == (pageLength+1)*pageWidth) {
				thisIndex = 1;
//				thisInner.children( "li:lt("+param.playNum+")" ).clone().appendTo(thisInner);
				changeInner(function() {
//					thisInner.scrollLeft(0);
					thisInner.css("margin-left", -pageWidth + "px");
//					thisInner.children("li:not(:lt(" + (thisInnerItem.length-param.playNum) + "))").remove();
				});
			} else {
				thisIndex = thisScroll / pageWidth;
				changeInner();
			}
		}
		
		/************************更换轮播************************/
		function changeInner(callback) {
			thisCarousel.find(".indicators li").removeClass("active");
			thisCarousel.find(".indicators li").eq(thisIndex-1).addClass("active");
			
			thisInner.stop(true, true);
			thisInner.animate({
//				scrollLeft: thisScroll + "px"
				marginLeft: "-" + thisScroll + "px"
			}, param.speed, function() {
				callback ? callback() : "";
			});
		}
		
		/************************鼠标悬浮暂停轮播************************/
		thisCarousel.hover(function() {
			if (param.isAuto) {
				stop();
			}
		}, function() {
			if (param.isAuto) {
				autoPlay();
			}
		});
		
		init();
	};
	
	/************************默认参数************************/
	$.fn.carousel.defaults = {
		speed			: 500,		//移动速度
		interval		: 10000,	//间隔时间
		playNum			: 1,		//播放数量
		isAuto			: false,	//自动播放状态
		showIndicators	: false,	//显示播放导航
		showControl		: false		//显示控制按钮
	};
})(jQuery);