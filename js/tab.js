(function() {
	var tab			= $("[data-role='tab']"),
		tabFrame	= $("[data-role='tabFrame']");
	
	//隐藏tabFrame
	function hideFrameItem(attrs) {
		tabFrame.find(attrs).removeClass("show_thead");
		tabFrame.find(attrs).hide();
	}
	
	//显示tabFrame
	function showFrameItem(attrs) {
		tabFrame.find(attrs).eq(0).addClass("show_thead");
		tabFrame.find(attrs).show();
	}
	
	//tab点击事件
	tab.find("[data-link]").click(function() {
		tab.find("[data-link]").not($(this)).removeClass("selected");
		$(this).addClass("selected");
		hideFrameItem("[data-name]");
		
		if ($(this).attr("data-link") == "all") {
			showFrameItem("[data-name]");
		} else {
			showFrameItem("[data-name='" + $(this).attr("data-link") + "']");
		}
	});
	
	//初始化第一个tab
	tab.find("[data-link]").eq(0).click();
})();
