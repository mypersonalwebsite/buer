(function() {
	var filtrate	= $("[data-role='filtrate']"),
		beSelected	= $("[data-role='filtrate']").find("[data-role='beSelected']"),
		toSelected	= $("[data-role='filtrate']").find("[data-role='toSelected']"),
		actions		= $("[data-role='filtrate']").find("[data-action]");
	
	actions.click(function() {
		var thisObj	 = $(this),
			method	 = $(this).data("action");
		
		if (method == "more") {
			thisObj.toggleClass("show_more");
			thisObj.parents("[data-role='filtrate']").find(".brand").toggleClass("more");
		} else if (method == "multiple") {
			thisObj.parents("[data-role='filtrate']").find(".brand").addClass("multiple");
			console.log(thisObj.parents("[data-role='filtrate']").find(".brand input[type='checkbox']"));
			thisObj.parents("[data-role='filtrate']").find(".brand input[type='checkbox']").attr("checked", false);
		} else if (method == "cancel") {
			thisObj.parents("[data-role='filtrate']").find(".brand").removeClass("multiple");
		} else if (method == "delete") {
			thisObj.parent().remove();
		} else if (method == "add") {
			//已打开多选
			if (thisObj.parents("[data-role='filtrate']").find(".brand").hasClass("multiple")) {
				var obj = $("<li><span>" + thisObj.find("span").text() + "</span><i class=\"iconfont icon-close\"></i></li>");
				toSelected.append(obj);
				
				obj.children("i").on("click", function() {
					$(this).parent().remove();
				});
				
			//未打开多选
			} else {
				var obj = $("<li><span>" + thisObj.find("span").text() + "</span><i class=\"iconfont icon-close\"></i></li>");
				beSelected.append(obj);
				console.log(obj);
				
				obj.children("i").on("click", function() {
					
					$(this).parent().remove();
				});
			}
		} else {
			alert("方法错误！");
		}
	});
})();
