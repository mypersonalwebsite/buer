(function() {
	var provinceObj	= $("[data-src='province']"),
		cityObj		= $("[data-src='city']"),
		districtObj	= $("[data-src='district']");
	
	/*创建选项*/
	function createOption(toObj, val, text) {
		var obj = document.createElement("li");
		obj.setAttribute("data-val", val);
		obj.innerHTML = text;
		toObj.append(obj);
	}
	
	/*清除选项*/
	function removeOption(obj) {
		obj.children("ul").find("li:gt(0)").remove();
		obj.children("input[type='text']").val(obj.children("ul").children("li").eq(0).html());
		obj.children("input[type='hidden']").val(obj.children("ul").children("li").eq(0).attr("data-val"));
	}
	
	/*选中当前选项*/
	function selectVal(obj) {
		var thisTextObj	= obj.parent().parent().children("input[type='text']"),
			thisValObj	= obj.parent().parent().children("input[type='hidden']");
		
		thisTextObj.val(obj.html());
		thisValObj.val(obj.attr("data-val"));
		
		return thisValObj.val();	//返回已选中的值
	}
	
	/*创建初始选项*/
	if (provinceObj.find("li").length<=0) {
		createOption(provinceObj.children("ul"), "0", "省份")	;
		selectVal(provinceObj.find("li").eq(0));
	}
	if (cityObj.find("li").length<=0) {
		createOption(cityObj.children("ul"), "0", "城市")	;
		selectVal(cityObj.find("li").eq(0));
	}
	if (districtObj.find("li").length<=0) {
		createOption(districtObj.children("ul"), "0", "区/县")	;
		selectVal(districtObj.find("li").eq(0));
	}
	
	/*载入所有省份选项*/
	$.each(province, function(index, data) {
		createOption(provinceObj.children("ul"), data.ProID, data.ProName);
	});
	
	/*绑定省份选择事件*/
	provinceObj.find("li").on("click", function() {
		var thisCityObj		 = $(this).parents("[data-src]").parent().find("[data-src='city']"),
			thisDistrictObj	 = $(this).parents("[data-src]").parent().find("[data-src='district']"),
			thisVal			 = selectVal($(this));
		removeOption(thisCityObj);
		removeOption(thisDistrictObj);
		
		if (thisVal>0) {
			/*载入当前选择省份的所有城市选项*/
			$.each(city, function(index, data) {
				if (thisVal == data.ProID) {
					createOption(thisCityObj.children("ul"), data.CityID, data.CityName);
				}
			});
			selectVal(thisCityObj.find("li").eq(1));	//自动选中第一个城市
			
			/*载入第一个城市的所有区/县选项*/
			$.each(district, function(index, data) {
				if (thisCityObj.find("li").eq(1).attr("data-val") == data.CityID) {
					createOption(thisDistrictObj.children("ul"), data.Id, data.DisName);
				}
			});
			selectVal(thisDistrictObj.find("li").eq(1));	//自动选中第一个区/县
			
			/*绑定城市选择事件*/
			thisCityObj.find("li").on("click", function() {
				var thisVal = selectVal($(this));
				removeOption(thisDistrictObj);
				
				if (thisVal>0) {
					/*载入当前选择城市的所有区/县选项*/
					$.each(district, function(index, data) {
						if (thisVal == data.CityID) {
							createOption(thisDistrictObj.children("ul"), data.Id, data.DisName);
						}
					});
					selectVal(thisDistrictObj.find("li").eq(1));	//自动选中第一个区/县
					
					/*绑定区/县选择事件*/
					thisDistrictObj.find("li").on("click", function() {
						selectVal($(this));
					});
				}
			});
			
			/*绑定区/县选择事件*/
			thisDistrictObj.find("li").on("click", function() {
				selectVal($(this));
			});
		}
	});
})();