(function() {
	var selects = $("[data-role='select']");
	
	/*选中默认值*/
	selects.each(function() {
		var optionSelected	= $(this).find("[data-selected]"),	//选中的选项
			optionFirst		= $(this).find("li:first-child");			//第一个选项
		
		if (optionSelected.length) {	//判断是否存在选中的选项
			/*选中的选项内容填入表单*/
			$(this).find("input[type='text']").val(optionSelected.html());
			$(this).find("input[type='hidden']").val(optionSelected.attr("data-val"));
		} else {
			/*第一个选项内容填入表单*/
			$(this).find("input[type='text']").val(optionFirst.html());
			$(this).find("input[type='hidden']").val(optionFirst.attr("data-val"));
		}
	});
	
	/*点击空白处隐藏下拉框选项*/
	$(document).click(function() {
		selects.find("ul").removeClass("show");
	});

	/*显示当前下拉框选项*/
	selects.find("i").click(function(e)　 {
		selects.not($(this).parents("[data-role='select']")).find("ul").removeClass("show");
		$(this).parents("[data-role='select']").find("ul").hasClass("show") ? $(this).parents("[data-role='select']").find("ul").removeClass("show") : $(this).parents("[data-role='select']").find("ul").addClass("show");
		e.stopPropagation();	//阻止冒泡到body
	});

	/*选中的选项内容*/
	selects.find("li").click(function() {
		$(this).parents("[data-role='select']").find("input[type='text']").val($(this).html());
		$(this).parents("[data-role='select']").find("input[type='hidden']").val($(this).attr("data-val"));
	});
})();