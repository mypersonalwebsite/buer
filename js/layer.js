(function() {
	var ents			 = $("[data-ent]"),						//弹出层入口
		layer			 = $(".layer"),							//弹出层组件
		dialogs			 = $(".layer .dialogs"),				//对话框
		actions			 = $(".layer .dialogs [data-action]"),	//对话框操作入口
		thisDialogs		 = null,								//打开的对话框
		thisHeight		 = 0;									//打开的对话框的高度
		thisOtherHeight	 = 0;									//打开的对话框的除内容外的高度
	
	/************************设置弹出层高度************************/
	function setHeight() {
		
		//打开对话框时
		if (thisDialogs != null) {
			
			//当对话框高度大于浏览器窗口高度时
			if (thisHeight > $(window).height()) {
				thisDialogs.find(".content").height( $(window).height() - thisOtherHeight );
				
			//正常情况时
			} else {
				thisDialogs.find(".content").removeAttr("style");
			}
		}
	}
	
	/************************打开弹出层************************/
	ents.click(function() {
		thisDialogs		 = $("#"+$(this).data("ent"));
		layer.css("visibility", "visible");
		thisDialogs.css("display", "block");
		thisHeight		 = thisDialogs.height();
		thisOtherHeight	 = thisDialogs.height() - thisDialogs.find(".content").height();
		setHeight();
		$("html").css("overflow", "hidden");
		return false;
	});
	
	/************************组件操作************************/
	actions.click(function() {
		
		//关闭弹出层
		if ($(this).data("action") == "close") {
			$(this).parents(".dialogs").removeAttr("style");
			layer.removeAttr("style");
			thisDialogs.find(".content").removeAttr("style");
			thisDialogs = null;
			$("html").removeAttr("style");
			return false;
			
		//其它情况
		} else {
			alert("参数错误！");
		}
	});
	
	/************************改变浏览器窗口************************/
	$(window).resize(function() {
		setHeight();
	});
})();

$(function(){
    $(".switch_sex>i").click(function(){
        $(".switch_sex>div").toggleClass("show");
    });
});